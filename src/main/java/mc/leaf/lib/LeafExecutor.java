package mc.leaf.lib;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class LeafExecutor {

    private Object on;
    private List<Object> args;
    private Method target;
    private boolean playerOnly;

    public LeafExecutor(Object on, List<Object> args, Method target, boolean playerOnly) {
        this.on = on;
        this.args = args;
        this.target = target;
        this.playerOnly = playerOnly;
    }

    public boolean execute(CommandSender sender) throws InvocationTargetException, IllegalAccessException {
        if (playerOnly && !(sender instanceof Player)) {
            sender.sendMessage("§4§lOnly a player can use this.");
            return true;
        }
        List<Object> params = new ArrayList<>();
        params.add(playerOnly ? ((Player) sender) : sender);
        params.addAll(this.args);
        return (boolean) target.invoke(on, params.toArray());
    }
}
