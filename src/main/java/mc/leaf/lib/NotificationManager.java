package mc.leaf.lib;

import mc.leaf.LeafPlugin;
import mc.leaf.abstracts.LeafManager;
import mc.leaf.abstracts.notifications.ProgressNotification;
import mc.leaf.enums.NotificationState;
import mc.leaf.interfaces.notifications.INotification;

import java.util.ArrayList;
import java.util.List;

public class NotificationManager extends LeafManager<INotification> {

    public NotificationManager(LeafPlugin leaf) {
        super(leaf);
        this.start();
    }

    /**
     * Called when this manager is started.
     */
    @Override
    public void onStart() {

    }

    /**
     * Called when this manager is stopped.
     */
    @Override
    public void onStop() {
        this.repeatingEntityList.forEach(INotification::dispose);
        this.repeatingEntityList.clear();
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        List<INotification> notifications = new ArrayList<>();

        while (!this.repeatingEntityList.isEmpty()) {
            INotification notification = this.repeatingEntityList.poll();

            notification.run();

            if (notification instanceof ProgressNotification) {
                ProgressNotification progressNotification = (ProgressNotification) notification;

                if (progressNotification.getProgression() >= 1 && !progressNotification.continuousDisplay()) {
                    progressNotification.setState(NotificationState.FINISHED);
                }

                if (progressNotification.getState().postpone) {
                    notifications.add(progressNotification);
                }
            }
        }

        notifications.forEach(this::offer);
    }
}
