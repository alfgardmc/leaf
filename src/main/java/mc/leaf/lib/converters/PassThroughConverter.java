package mc.leaf.lib.converters;

import mc.leaf.lib.ArgumentConverter;
import org.jetbrains.annotations.NotNull;

public class PassThroughConverter implements ArgumentConverter<String> {

    @Override
    @NotNull
    public String output(String input) {
        return input;
    }

}
