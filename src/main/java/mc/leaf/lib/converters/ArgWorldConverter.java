package mc.leaf.lib.converters;

import mc.leaf.lib.ArgumentConverter;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.jetbrains.annotations.Nullable;

public class ArgWorldConverter implements ArgumentConverter<World> {

    @Override
    @Nullable
    public World output(String input) {
        World world = null;
        for (World serverWorld : Bukkit.getWorlds()) {
            if (serverWorld.getName().equalsIgnoreCase(input)) {
                world = serverWorld;
                break;
            }
        }
        return world;
    }
}
