package mc.leaf.lib.converters;

import mc.leaf.lib.ArgumentConverter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

public class ArgPlayerConverter implements ArgumentConverter<Player> {

    @Nullable
    @Override
    public Player output(String input) {
        return Bukkit.getPlayer(input);
    }

}
