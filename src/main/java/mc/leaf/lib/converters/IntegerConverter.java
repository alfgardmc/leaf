package mc.leaf.lib.converters;

import mc.leaf.lib.ArgumentConverter;
import org.jetbrains.annotations.Nullable;

public class IntegerConverter implements ArgumentConverter<Integer> {

    @Nullable
    @Override
    public Integer output(String input) {
        return Integer.parseInt(input);
    }
}
