package mc.leaf.lib;

import java.io.File;
import java.sql.*;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public abstract class Storage {

    private final List<String> REQUESTS_INIT = Collections.singletonList(
            "CREATE TABLE IF NOT EXISTS data (`name` TEXT NOT NULL UNIQUE PRIMARY KEY, `value` TEXT NOT NULL)"
    );

    private Connection connection;

    public Storage() throws Exception {
        if (!this.getStorageFile().exists()) {
            if (!this.getStorageFile().createNewFile()) {
                throw new Exception("Storage file couldn't be created.");
            }
        }

        this.openConnection();

        for (String sql : REQUESTS_INIT) {
            this.executeRaw(sql);
        }
    }

    public abstract File getStorageFile();

    private void openConnection() throws Exception {
        String url = "jdbc:sqlite:" + this.getStorageFile().getAbsolutePath();
        this.connection = DriverManager.getConnection(url);
    }

    private void checkConnection() throws Exception {
        if (connection == null || connection.isClosed() || !connection.isValid(1)) {
            this.openConnection();
        }
    }

    private void executeRaw(String sql) throws Exception {
        this.checkConnection();
        Statement statement = this.connection.createStatement();
        statement.execute(sql);
        statement.close();
    }

    public String store(String name, String value) throws Exception {
        this.checkConnection();
        PreparedStatement statement = this.connection.prepareStatement("INSERT OR REPLACE INTO data VALUES (?, ?)");

        statement.setString(1, name);
        statement.setString(2, value);

        statement.execute();
        statement.close();
        return value;
    }

    public Optional<String> retrieve(String name) throws Exception {
        this.checkConnection();
        PreparedStatement statement = this.connection.prepareStatement("SELECT value FROM data WHERE name = ?");
        statement.setString(1, name);

        ResultSet rs = statement.executeQuery();
        Optional<String> result;

        if (rs.next()) {
            result = Optional.of(rs.getString("value"));
        } else {
            result = Optional.empty();
        }

        rs.close();
        statement.close();
        return result;
    }

}
