package mc.leaf.lib.annotations;

import mc.leaf.lib.ArgumentConverter;
import mc.leaf.lib.converters.PassThroughConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Param {

    String value();

    Class<? extends ArgumentConverter> convert() default PassThroughConverter.class;

}
