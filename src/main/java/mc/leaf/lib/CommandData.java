package mc.leaf.lib;


import mc.leaf.lib.annotations.Param;
import mc.leaf.lib.annotations.PlayerOnly;
import mc.leaf.lib.annotations.Syntax;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandData {

    private Object on;

    public CommandData(Object on) {
        this.on = on;
    }

    public LeafExecutor getExecutor(String commandString) throws Exception {
        for (Method method : on.getClass().getMethods()) {
            Syntax syntax = method.getAnnotation(Syntax.class);
            if (syntax != null) {
                Pattern commandPattern = Pattern.compile(syntax.value());
                Matcher matcher = commandPattern.matcher(commandString);

                if (matcher.matches()) {

                    List<Object> params = new ArrayList<>();

                    for (Parameter parameter : method.getParameters()) {
                        Param arg = parameter.getAnnotation(Param.class);
                        if (arg == null) {
                            throw new Exception("Unbound argument : " + parameter.getName() + " on " + method.getName());
                        }
                        if (arg.value().startsWith("=")) {
                            continue; // Ignore it, it's probably the sender.
                        }

                        String value = matcher.group(arg.value());

                        Class<? extends ArgumentConverter> converter = arg.convert();
                        ArgumentConverter converterInstance = converter.newInstance();
                        params.add(converterInstance.output(value));
                    }

                    return new LeafExecutor(this.on, params, method, method.getAnnotation(PlayerOnly.class) != null);
                }
            }
        }
        return null;
    }

}
