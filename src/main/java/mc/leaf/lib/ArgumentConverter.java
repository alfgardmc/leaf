package mc.leaf.lib;

import org.jetbrains.annotations.Nullable;

public interface ArgumentConverter<OUT> {

    @Nullable
    OUT output(String input);

}
