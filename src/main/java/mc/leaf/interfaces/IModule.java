package mc.leaf.interfaces;

/**
 * Interface representing a module of the Leaf library. Use this when you want to code a plugin using Leaf's feature
 * such as notification or easy command declaration.
 */
public interface IModule {

    void enable(boolean startup);

    void disable(boolean shutdown);

    void onEnable() throws Exception;

    void onDisable() throws Exception;

    String getName();

}
