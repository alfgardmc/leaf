package mc.leaf.interfaces;

/**
 * Interface representing a timed manager.
 */
public interface IManager<T> extends Runnable {

    /**
     * Offer an entity to this manager.
     *
     * @param offer The entity to offer
     */
    void offer(T offer);

    /**
     * Start the manager.
     */
    void start();

    /**
     * Stop the manager.
     */
    void stop();

    /**
     * Called when this manager is started.
     */
    void onStart();

    /**
     * Called when this manager is stopped.
     */
    void onStop();

}
