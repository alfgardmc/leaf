package mc.leaf.interfaces;

/**
 * Interface representing a cancellable entity. (ex: Notification, Task...)
 */
public interface ICancellable {

    /**
     * Tell the current object entity to cancel what it's actually doing.
     */
    void cancel();

    /**
     * Called when {@link #cancel()} has finished its execution flow.
     */
    void onCancel();

}
