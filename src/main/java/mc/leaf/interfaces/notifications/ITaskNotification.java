package mc.leaf.interfaces.notifications;

import mc.leaf.enums.NotificationState;
import mc.leaf.interfaces.ICancellable;

/**
 * Interface representing a continuous notification used to provide visual
 * feedback of the progression from a long running task.
 */
public interface ITaskNotification extends INotification, ICancellable {

    /**
     * Double value between 0 and 1 (inclusive) representing the percentage completed on the current task.
     * Any overflow or underflow will be automatically clamped.
     * <p>
     * Examples :
     * <ul>
     * <li>Returning 1.4 will result in 1
     * <li>Returning -0.1 will result in 0
     * </ul>
     * <p>
     * If this method returns 1, this task will be automatically finished.
     * <p>
     *
     * <b>NOTE</b> : This will be always called in a synchronous way very frequently.
     * <b>Don't make heavy calculation here.</b>
     *
     * @return The current progression of the task.
     */
    double getProgression();

    /**
     * Retrieve the current state of this notification.
     *
     * @return The state for this notification.
     * @see NotificationState
     */
    NotificationState getState();

    /**
     * Define the current state of this notification. If the state provided is the same as {@link #getState()}, the method
     * will be exited immediately.
     * <p>
     * Depending on the state provided, some method could be called :
     * <ul>
     *     <li>{@link NotificationState#RUNNING} will call {@link #onTaskStarted()}</li>
     *     <li>{@link NotificationState#FINISHED} will call {@link #onTaskFinished()}</li>
     *     <li>{@link NotificationState#CANCELLED} will call {@link #onTaskCancelled()}</li>
     *     <li>{@link NotificationState#WAITING} is the default state of a notification and won't trigger any method.</li>
     * </ul>
     *
     * @param state The new state for this notification.
     */
    void setState(NotificationState state);

    /**
     * Called when the task starts.
     */
    void onTaskStarted();

    /**
     * Called when the task is cancelled.
     */
    void onTaskCancelled();

    /**
     * Called when the task is finished.
     */
    void onTaskFinished();

    /**
     * Called every x ticks until {@link #onTaskCancelled()} or {@link #onTaskFinished()} is called,
     * where x varies depending on the TPS integer truncated value.
     * <pre>
     *     x = (20 - tps) + 1
     * </pre>
     * For instance, if your server runs at 17.4 TPS, it will be truncated to 17 TPS,
     * so this method will be called every 4 ticks.
     * <p>
     * This is mainly to reduce work-load when your server is facing lag-spikes.
     * <p>
     * <b>NOTE</b> : The way x is calculated could be changed in the future (for performance balancing purpose).
     */
    void tick();

}
