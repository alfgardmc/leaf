package mc.leaf.interfaces.notifications;

import org.bukkit.Keyed;

/**
 * Interface representing a notification.
 */
public interface INotification extends Keyed, Runnable {

    /**
     * Display the notification to players.
     */
    void show();

    /**
     * Hide the notification from players.
     */
    void hide();

    /**
     * Dispose of every resource that need to be freed.
     * (Ex: Removing a BossBar that was used to show the notification)
     */
    void dispose();

    /**
     * Retrieve the text to use for this notification when displayed to players
     *
     * @return The text to use for this notification.
     */
    String getText();
}
