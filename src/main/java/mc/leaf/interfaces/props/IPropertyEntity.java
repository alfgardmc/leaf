package mc.leaf.interfaces.props;

public interface IPropertyEntity {

    void loadStorage();

    void saveStorage();

}
