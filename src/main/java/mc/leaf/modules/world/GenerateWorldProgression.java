package mc.leaf.modules.world;

import mc.leaf.abstracts.notifications.ProgressNotification;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;

public class GenerateWorldProgression extends ProgressNotification {

    private WorldModule module;
    private World world;

    private int x1;
    private int z1;
    private int x2;
    private int z2;

    private int currentX;
    private int currentZ;

    private int totalChunksCount;
    private int actualGenerated = 0;
    private int speed = 10;

    private int skipTo = 0;

    public GenerateWorldProgression(WorldModule module, World world, int x1, int z1, int x2, int z2) {
        super(module.getPlugin());

        this.module = module;

        this.world = world;
        this.x1 = Math.min(x1, x2);
        this.z1 = Math.min(z1, z2);
        this.x2 = Math.max(x1, x2);
        this.z2 = Math.max(z1, z2);

        int xCount = Math.abs(x1) + Math.abs(x2);
        int zCount = Math.abs(z1) + Math.abs(z2);

        this.currentX = this.x1;
        this.currentZ = this.z1;

        this.totalChunksCount = zCount * xCount;
    }

    public void setSkipTo(int skipTo) {
        this.skipTo = skipTo;
    }

    public int getActualGenerated() {
        return actualGenerated;
    }

    public int getTotalChunksCount() {
        return totalChunksCount;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * Double value between 0 and 1 (inclusive) representing the percentage completed on the current task.
     * Any overflow or underflow will be automatically clamped.
     * <p>
     * Examples :
     * <ul>
     * <li>Returning 1.4 will result in 1
     * <li>Returning -0.1 will result in 0
     * </ul>
     * <p>
     * If this method returns 1, this task will be automatically finished.
     * <p>
     *
     * <b>NOTE</b> : This will be always called in a synchronous way very frequently.
     * <b>Don't make heavy calculation here.</b>
     *
     * @return The current progression of the task.
     */
    @Override
    public double getProgression() {
        return (double) this.actualGenerated / (double) this.totalChunksCount;
    }

    /**
     * Called when the task starts.
     */
    @Override
    public void onTaskStarted() {
        Bukkit.broadcastMessage("§6§l[Leaf] §fLa génération du monde a commencé. Cela va prendre un certain temps et des ressources serveurs.");
    }

    /**
     * Called when the task is cancelled.
     */
    @Override
    public void onTaskCancelled() {
        Bukkit.broadcastMessage("§6§l[Leaf] §fLa génération du monde a été annulée.");
        this.dispose();
        this.module.removeGenerator(this.world);
    }

    /**
     * Called when the task is finished.
     */
    @Override
    public void onTaskFinished() {
        Bukkit.broadcastMessage("§6§l[Leaf] §fLa génération du monde a été terminée.");
        this.dispose();
        this.module.removeGenerator(this.world);
    }

    /**
     * Called every x ticks until {@link #onTaskCancelled()} or {@link #onTaskFinished()} is called,
     * where x varies depending on the TPS integer truncated value.
     * <pre>
     *     x = (20 - tps) + 1
     * </pre>
     * For instance, if your server runs at 17.4 TPS, it will be truncated to 17 TPS,
     * so this method will be called every 4 ticks.
     * <p>
     * This is mainly to reduce work-load when your server is facing lag-spikes.
     * <p>
     * <b>NOTE</b> : The way x is calculated could be changed in the future (for performance balancing purpose).
     */
    @Override
    public void tick() {
        this.getBossBar().setTitle(this.getText());

        for (int i = 0; i < speed; i++) {

            if (this.currentZ <= this.z2) {

                if (this.skipTo <= this.actualGenerated) {
                    Chunk chunk = world.getChunkAt(currentX, currentZ);

                    chunk.load(true);
                    chunk.unload(true);
                }

                this.actualGenerated++;
                this.currentX++;

                if (this.currentX > this.x2) {
                    this.currentX = this.x1;
                    this.currentZ++;
                }
            } else {
                this.actualGenerated = this.totalChunksCount; // Force completion.
            }
        }
    }

    /**
     * Retrieve the text to use for this notification when displayed to players
     *
     * @return The text to use for this notification.
     */
    @Override
    public String getText() {
        return "§lGénération... " + Math.round((this.getProgression() * 100)) + "%";
    }
}
