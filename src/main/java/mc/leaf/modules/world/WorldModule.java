package mc.leaf.modules.world;

import mc.leaf.LeafPlugin;
import mc.leaf.abstracts.LeafModule;
import mc.leaf.lib.NotificationManager;
import org.bukkit.World;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.player.PlayerChangedWorldEvent;

import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

public class WorldModule extends LeafModule {

    private HashMap<UUID, GenerateWorldProgression> generators = new HashMap<>();

    public WorldModule(LeafPlugin plugin) {
        super(plugin, "World");
    }

    @Override
    public void onEnable() {
        this.info("Registering commands...");
        this.registerCommands();
    }

    private void registerCommands() {
        PluginCommand command = this.getPlugin().getCommand("worldgen");

        if (command != null) {
            WorldgenCommand worldgenCommand = new WorldgenCommand(this);
            command.setExecutor(worldgenCommand);
            command.setTabCompleter(worldgenCommand);
        }
    }

    /**
     * Retrieves the generator of the provided world.
     *
     * @param world The world from which the task should be retrieved.
     * @return An optional {@link GenerateWorldProgression}. If present, a task exists for the provided world.
     */
    public Optional<GenerateWorldProgression> getGenerator(World world) {
        if (this.generators.containsKey(world.getUID())) {
            return Optional.of(this.generators.get(world.getUID()));
        }
        return Optional.empty();
    }

    /**
     * Create a new generation task for the provided world. Only one can be started per World.
     * If a task can't be started, an empty optional value will be returned.
     *
     * @param world       The world on which the task should be started.
     * @param chunkStartX X coordinate of the starting chunk.
     * @param chunkStartZ Z coordinate of the starting chunk.
     * @param chunkEndX   X coordinate of the final chunk.
     * @param chunkEndZ   Z coordinate of the final chunk.
     * @return An optional {@link GenerateWorldProgression}. If present, the task can be started.
     */
    public Optional<GenerateWorldProgression> createGenerator(World world, int chunkStartX, int chunkStartZ, int chunkEndX, int chunkEndZ) {
        if (this.getGenerator(world).isPresent()) {
            return Optional.empty();
        }

        GenerateWorldProgression progression = new GenerateWorldProgression(this, world, chunkStartX, chunkStartZ, chunkEndX, chunkEndZ);
        this.getNotificationManager().offer(progression);
        return Optional.of(progression);
    }

    /**
     * Delete the task linked to the provided world. Please insure that the task is finished / cancelled before calling this.
     *
     * @param world
     */
    public void removeGenerator(World world) {
        if (this.getGenerator(world).isPresent()) {
            this.generators.remove(world.getUID());
        }
    }


    /**
     * Retrieves the Notification Manager of the plugin.
     *
     * @return A {@link NotificationManager} instance.
     */
    public NotificationManager getNotificationManager() {
        return this.getPlugin().getNotificationManager();
    }


    @Override
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
        if (!event.getPlayer().isOp()) {
            return;
        }

        Optional<GenerateWorldProgression> optionalTask = this.getGenerator(event.getFrom());

        if (optionalTask.isPresent()) {
            GenerateWorldProgression task = optionalTask.get();
            task.getBossBar().removePlayer(event.getPlayer());
        }

        optionalTask = this.getGenerator(event.getPlayer().getWorld());

        if (optionalTask.isPresent()) {
            GenerateWorldProgression task = optionalTask.get();
            task.getBossBar().addPlayer(event.getPlayer());
        }
    }
}
