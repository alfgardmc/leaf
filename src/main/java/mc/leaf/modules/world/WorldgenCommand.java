package mc.leaf.modules.world;

import mc.leaf.abstracts.LeafCommand;
import mc.leaf.enums.NotificationState;
import mc.leaf.lib.annotations.Param;
import mc.leaf.lib.annotations.PlayerOnly;
import mc.leaf.lib.annotations.Syntax;
import mc.leaf.lib.converters.IntegerConverter;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public class WorldgenCommand extends LeafCommand implements TabCompleter {

    private final WorldModule module;

    public WorldgenCommand(WorldModule module) {
        this.module = module;
    }

    public WorldModule getModule() {
        return module;
    }

    @PlayerOnly
    @Syntax("run (?<x1>-?[0-9]*) (?<z1>-?[0-9]*) (?<x2>-?[0-9]*) (?<z2>-?[0-9]*)")
    public void startWorldGeneration(@Param("=player") Player player, @Param(value = "x1", convert = IntegerConverter.class) Integer chunkStartX, @Param(value = "z1", convert = IntegerConverter.class) Integer chunkStartZ, @Param(value = "x2", convert = IntegerConverter.class) Integer chunkEndX, @Param(value = "z2", convert = IntegerConverter.class) Integer chunkEndZ) {

        if (!player.isOp()) {
            this.getModule().sendMessage("§cPour gérer les tâches de génération, vous devez avoir les droits OP.", player);
            return;
        }
        Optional<GenerateWorldProgression> optionalTask = this.getModule().createGenerator(player.getWorld(), chunkStartX, chunkStartZ, chunkEndX, chunkEndZ);

        if (!optionalTask.isPresent()) {
            this.getModule().sendMessage("§cUne tâche est déjà en cours d'exécution pour ce monde. Veuillez l'annuler pour pouvoir en lancer une autre.");
            return;
        }

        GenerateWorldProgression task = optionalTask.get();
        task.setState(NotificationState.RUNNING);
        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> {
            if (onlinePlayer.isOp() && onlinePlayer.getWorld().equals(player.getWorld())) {
                task.getBossBar().addPlayer(onlinePlayer);
            }
        });

        this.getModule().sendMessage(String.format("§eUne tâche de génération pour le monde §7%s §ea été lancée.", player.getWorld().getName()));
    }


    /**
     * Requests a list of possible completions for a command argument.
     *
     * @param sender  Source of the command.  For players tab-completing a
     *                command inside of a command block, this will be the player, not
     *                the command block.
     * @param command Command which was executed
     * @param alias   The alias used
     * @param args    The arguments passed to the command, including final
     *                partial argument to be completed and command label
     * @return A List of possible completions for the final argument, or null
     * to default to the command executor
     */
    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if (!(sender instanceof Player)) {
            return Collections.singletonList("you_have_to_be_a_player");
        }
        Player player = ((Player) sender);

        List<String> firstArgs;

        if (this.getModule().getGenerator(player.getWorld()).isPresent()) {
            firstArgs = Arrays.asList("speed", "cancel");
        } else {
            firstArgs = Collections.singletonList("run");
        }

        List<String> actualPossible = firstArgs.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());

        if (actualPossible.size() == 1 && args.length > 1) {
            if ("run".equals(actualPossible.get(0))) {
                switch (args.length) {
                    case 2:
                        return Collections.singletonList(String.valueOf(player.getChunk().getX()));
                    case 3:
                        return Collections.singletonList(String.valueOf(player.getChunk().getZ()));
                    default:
                        return new ArrayList<>();
                }
            }
            return new ArrayList<>();
        } else {
            return actualPossible;
        }
    }
}
