package mc.leaf.modules.monitoring;

import mc.leaf.abstracts.LeafCommand;
import mc.leaf.lib.annotations.Param;
import mc.leaf.lib.annotations.PlayerOnly;
import mc.leaf.lib.annotations.Syntax;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class MonitoringCommand extends LeafCommand implements TabCompleter {

    private MonitoringModule module;

    public MonitoringCommand(MonitoringModule monitoringModule) {
        this.module = monitoringModule;
    }

    @PlayerOnly
    @Syntax("tps show")
    public boolean showTPS(@Param("=sender") Player player) {
        this.module.info(String.format("Showing TPS bar to %s.", player.getName()));
        this.module.getTps().getBossBar().addPlayer(player);
        return true;
    }

    @PlayerOnly
    @Syntax("tps hide")
    public boolean hideTPS(@Param("=sender") Player player) {
        this.module.info(String.format("Hiding TPS bar from %s.", player.getName()));
        this.module.getTps().getBossBar().removePlayer(player);
        return true;
    }


    /**
     * Requests a list of possible completions for a command argument.
     *
     * @param sender  Source of the command.  For players tab-completing a
     *                command inside of a command block, this will be the player, not
     *                the command block.
     * @param command Command which was executed
     * @param alias   The alias used
     * @param args    The arguments passed to the command, including final
     *                partial argument to be completed and command label
     * @return A List of possible completions for the final argument, or null
     * to default to the command executor
     */
    @Nullable
    @Override
    public List<String> onTabComplete(@NotNull CommandSender sender, @NotNull org.bukkit.command.Command command, @NotNull String alias, @NotNull String[] args) {
        return null;
    }
}
