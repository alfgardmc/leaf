package mc.leaf.modules.monitoring;

import mc.leaf.LeafPlugin;
import mc.leaf.abstracts.notifications.ProgressNotification;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;

import java.text.DecimalFormat;

public class MonitoringBar extends ProgressNotification {

    private String text;

    public MonitoringBar(LeafPlugin plugin) {
        super(plugin);
        this.text = "";
    }

    @Override
    public boolean continuousDisplay() {
        return true;
    }

    /**
     * Double value between 0 and 1 (inclusive) representing the percentage completed on the current task.
     * Any overflow or underflow will be automatically clamped.
     * <p>
     * Examples :
     * <ul>
     * <li>Returning 1.4 will result in 1
     * <li>Returning -0.1 will result in 0
     * </ul>
     * <p>
     * If this method returns 1, this task will be automatically finished.
     * <p>
     *
     * <b>NOTE</b> : This will be always called in a synchronous way very frequently.
     * <b>Don't make heavy calculation here.</b>
     *
     * @return The current progression of the task.
     */
    @Override
    public double getProgression() {
        return Bukkit.getTPS()[0] / 20.0;
    }

    /**
     * Called when the task starts.
     */
    @Override
    public void onTaskStarted() {

    }

    /**
     * Called when the task is cancelled.
     */
    @Override
    public void onTaskCancelled() {

    }

    /**
     * Called when the task is finished.
     */
    @Override
    public void onTaskFinished() {

    }

    /**
     * Called every x ticks until {@link #onTaskCancelled()} or {@link #onTaskFinished()} is called,
     * where x varies depending on the TPS integer truncated value.
     * <pre>
     *     x = (20 - tps) + 1
     * </pre>
     * For instance, if your server runs at 17.4 TPS, it will be truncated to 17 TPS,
     * so this method will be called every 4 ticks.
     * <p>
     * This is mainly to reduce work-load when your server is facing lag-spikes.
     * <p>
     * <b>NOTE</b> : The way x is calculated could be changed in the future (for performance balancing purpose).
     */
    @Override
    public void tick() {
        double tps = Bukkit.getTPS()[0];
        if (tps > 20) {
            tps = 20;
        }
        String tpsStr = new DecimalFormat("#.00").format(tps);

        this.text = "§lTPS : " + tpsStr;
        this.getBossBar().setTitle(this.getText());

        if (tps >= 0 && tps <= 10) {
            this.getBossBar().setColor(BarColor.RED);
        } else if (tps <= 15) {
            this.getBossBar().setColor(BarColor.YELLOW);
        } else {
            this.getBossBar().setColor(BarColor.GREEN);
        }

    }

    @Override
    public String getText() {
        return this.text;
    }
}
