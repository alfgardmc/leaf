package mc.leaf.modules.monitoring;

import mc.leaf.LeafPlugin;
import mc.leaf.abstracts.LeafModule;
import mc.leaf.enums.NotificationState;
import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;

public class MonitoringModule extends LeafModule {

    private MonitoringBar tps;

    public MonitoringModule(LeafPlugin plugin) {
        super(plugin, "Monitoring");
    }

    @Override
    public void onEnable() {
        this.info("Registering commands...");
        this.registerCommands();
        this.info("Registering notifications...");
        this.registerNotifications();
    }

    private void registerCommands() {
        PluginCommand command = Bukkit.getPluginCommand("monitoring");

        if (command != null) {
            MonitoringCommand monitoringCommand = new MonitoringCommand(this);
            command.setExecutor(monitoringCommand);
            command.setTabCompleter(monitoringCommand);
        }
    }

    private void registerNotifications() {
        this.tps = new MonitoringBar(this.getPlugin());
        this.getPlugin().getNotificationManager().offer(this.tps);
        this.tps.setState(NotificationState.RUNNING);
    }


    public MonitoringBar getTps() {
        return tps;
    }

}
