package mc.leaf.modules.tweaks;

import mc.leaf.LeafPlugin;
import mc.leaf.abstracts.LeafModule;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.data.Ageable;
import org.bukkit.block.data.BlockData;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This module tries to mimic a lot of feature of the mod VanillaTweaks.
 */
public class VanillaTweaksModule extends LeafModule {

    /**
     * List of Material that can be harvested with a hoe.
     */
    private Map<Material, Material> harvestable = Stream.of(new Material[][]{
            {Material.WHEAT, Material.WHEAT_SEEDS},
            {Material.BEETROOTS, Material.BEETROOT_SEEDS},
            {Material.CARROTS, Material.CARROT},
    }).collect(Collectors.collectingAndThen(
            Collectors.toMap(data -> data[0], data -> data[1]),
            Collections::unmodifiableMap)
    );

    /**
     * List of all Material valid as Shovel.
     */
    private List<Material> shovels = Arrays.asList(
            Material.WOODEN_SHOVEL,
            Material.STONE_SHOVEL,
            Material.IRON_SHOVEL,
            Material.GOLDEN_SHOVEL,
            Material.DIAMOND_SHOVEL
    );

    /**
     * List of all Material valid as Hoe
     */
    private List<Material> hoes = Arrays.asList(
            Material.WOODEN_HOE,
            Material.STONE_HOE,
            Material.IRON_HOE,
            Material.GOLDEN_HOE,
            Material.DIAMOND_HOE
    );

    /**
     * @param plugin Instance of the plugin
     */
    public VanillaTweaksModule(LeafPlugin plugin) {
        super(plugin, "VanillaTweaks");
    }


    @Override
    public void onEnable() {
        Bukkit.getServer().getPluginManager().registerEvents(this, this.getPlugin());
    }


    @Override
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return; // Ignore
        }

        if (event.getClickedBlock() == null) {
            return; // Ignore
        }

        if (event.getClickedBlock().getType() == Material.FARMLAND) {
            this.handleDirtRestore(event.getClickedBlock(), event.getItem());
        } else if (this.harvestable.containsKey(event.getClickedBlock().getType())) {
            this.handleHarvesting(event.getClickedBlock(), event.getItem());
        }
    }

    /**
     * Transform back a farmland if the item used is a shovel.
     *
     * @param block    The source block of this request
     * @param usedItem The item used when right clicking the block
     */
    private void handleDirtRestore(Block block, ItemStack usedItem) {
        if (usedItem == null) {
            return;
        }

        if (this.shovels.contains(usedItem.getType())) {
            block.setType(Material.DIRT);
        }
    }

    /**
     * Harvest a block on a farmland.
     *
     * @param block    The source block of this request
     * @param usedItem The item used when harvesting
     */
    private void handleHarvesting(Block block, ItemStack usedItem) {
        if (usedItem == null) {
            return;
        }

        if (this.hoes.contains(usedItem.getType())) {
            Collection<ItemStack> drops = block.getDrops(usedItem);
            BlockData data = block.getState().getBlockData();
            Ageable ageable = (Ageable) data;

            if (ageable.getAge() != ageable.getMaximumAge()) {
                return;
            }

            boolean doCrop = false;

            Material replant = this.harvestable.get(block.getType());

            for (ItemStack drop : drops) {
                if (drop.getType() == replant) {
                    drop.setAmount(drop.getAmount() - 1);
                    ageable.setAge(0);
                    doCrop = true;
                    break;
                }
            }

            if (doCrop) {
                for (ItemStack drop : drops) {
                    if (drop.getType() != Material.AIR && drop.getAmount() > 0) {
                        block.getWorld().dropItemNaturally(block.getLocation(), drop);
                    }
                }
                ageable.setAge(0);
                block.setBlockData(ageable);
            }
        }
    }


}
