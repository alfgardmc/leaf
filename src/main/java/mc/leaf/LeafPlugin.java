package mc.leaf;

import mc.leaf.abstracts.LeafModule;
import mc.leaf.lib.NotificationManager;
import mc.leaf.modules.monitoring.MonitoringModule;
import mc.leaf.modules.tweaks.VanillaTweaksModule;
import mc.leaf.modules.world.WorldModule;
import mc.leaf.players.LeafPlayer;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.boss.KeyedBossBar;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;
import java.util.logging.Level;

public class LeafPlugin extends JavaPlugin {

    private HashMap<Class<? extends LeafModule>, LeafModule> modules = new HashMap<>();
    private HashMap<UUID, LeafPlayer> players = new HashMap<>();
    private NotificationManager notificationManager;

    public <T extends LeafModule> T getModule(Class<T> module) {
        return module.cast(this.modules.get(module));
    }

    @Override
    public void onEnable() {
        this.modules.put(VanillaTweaksModule.class, new VanillaTweaksModule(this));
        this.modules.put(MonitoringModule.class, new MonitoringModule(this));
        this.modules.put(WorldModule.class, new WorldModule(this));

        this.notificationManager = new NotificationManager(this);
        this.notificationManager.start();

        // Load all module
        this.modules.forEach((clazz, module) -> module.enable(true));
    }

    @Override
    public void onDisable() {
        this.modules.forEach((clazz, module) -> module.disable(true));

        this.notificationManager.stop();

        // Remove left-over Bossbar
        Iterator<KeyedBossBar> it = Bukkit.getBossBars();
        while (it.hasNext()) {
            KeyedBossBar kbb = it.next();
            if (kbb.getKey().getNamespace().equals(this.getName().toLowerCase(Locale.ROOT))) {
                Bukkit.removeBossBar(kbb.getKey());
            }
        }

        this.getLogger().log(Level.INFO, "Saving players...");
    }

    public NamespacedKey getRandomKey() {
        StringBuilder builder = new StringBuilder();

        while (builder.length() < 8) {
            int rnd = new Random().nextInt(16);
            builder.append(Integer.toHexString(rnd).toLowerCase());
        }

        return new NamespacedKey(this, builder.toString());
    }

    public NotificationManager getNotificationManager() {
        return notificationManager;
    }

}
