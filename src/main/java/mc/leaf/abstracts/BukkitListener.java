package mc.leaf.abstracts;

import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.event.raid.*;
import org.bukkit.event.server.*;
import org.bukkit.event.vehicle.*;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.ThunderChangeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.weather.WeatherEvent;
import org.bukkit.event.world.*;

public abstract class BukkitListener implements Listener {

    public void onAreaEffectCloudApply(AreaEffectCloudApplyEvent event) {
    }

    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
    }

    public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
    }

    public void onBatToggleSleep(BatToggleSleepEvent event) {
    }

    public void onBlock(BlockEvent event) {
    }

    public void onBlockBreak(BlockBreakEvent event) {
    }

    public void onBlockBurn(BlockBurnEvent event) {
    }

    public void onBlockCanBuild(BlockCanBuildEvent event) {
    }

    public void onBlockCook(BlockCookEvent event) {
    }

    public void onBlockDamage(BlockDamageEvent event) {
    }

    public void onBlockDispense(BlockDamageEvent event) {
    }

    public void onBlockDispense(BlockDispenseEvent event) {
    }

    public void onBlockDispenseArmor(BlockDispenseArmorEvent event) {
    }

    public void onBlockDropItem(BlockDropItemEvent event) {
    }

    public void onBlockExp(BlockExpEvent event) {
    }

    public void onBlockExplode(BlockExplodeEvent event) {
    }

    public void onBlockFade(BlockFadeEvent event) {
    }

    public void onBlockFertilize(BlockFadeEvent event) {
    }

    public void onBlockForm(BlockFormEvent event) {
    }

    public void onBlockFromTo(BlockFromToEvent event) {
    }

    public void onBlockGrow(BlockGrowEvent event) {
    }

    public void onBlockIgnite(BlockIgniteEvent event) {
    }

    public void onBlockMultiPlace(BlockMultiPlaceEvent event) {
    }

    public void onBlockPhysics(BlockPhysicsEvent event) {
    }

    public void onBlockPistonExtend(BlockPistonExtendEvent event) {
    }

    public void onBlockPistonRetract(BlockPistonRetractEvent event) {
    }

    public void onBlockPlace(BlockPlaceEvent event) {
    }

    public void onBlockRedstone(BlockRedstoneEvent event) {
    }

    public void onBlockShearEntity(BlockShearEntityEvent event) {
    }

    public void onBlockSpread(BlockSpreadEvent event) {
    }

    public void onBrew(BrewEvent event) {
    }

    public void onBrewingStandFuel(BrewingStandFuelEvent event) {
    }

    public void onBroadcastMessage(BroadcastMessageEvent event) {
    }

    public void onCauldronLevelChange(CauldronLevelChangeEvent event) {
    }

    public void onChunk(ChunkEvent event) {
    }

    public void onChunkLoad(ChunkLoadEvent event) {
    }

    public void onChunkPopulate(ChunkPopulateEvent event) {
    }

    public void onChunkUnload(ChunkUnloadEvent event) {
    }

    public void onCraftItem(CraftItemEvent event) {
    }

    public void onCreatureSpawn(CreatureSpawnEvent event) {
    }

    public void onCreeperPower(CreeperPowerEvent event) {
    }

    public void onEnchantItem(EnchantItemEvent event) {
    }

    public void onEnderDragonChangePhase(EnderDragonChangePhaseEvent event) {
    }

    public void onEntity(EntityEvent event) {
    }

    public void onEntityAirChange(EntityAirChangeEvent event) {
    }

    public void onEntityBlockForm(EntityBlockFormEvent event) {
    }

    public void onEntityBreakDoorEvent(EntityBreakDoorEvent event) {
    }

    public void onEntityBreed(EntityBreedEvent event) {
    }

    public void onEntityChangeBlock(EntityChangeBlockEvent event) {
    }

    public void onEntityCombust(EntityCombustEvent event) {
    }

    public void onEntityCombustByBlock(EntityCombustByBlockEvent event) {
    }

    public void onEntityCombustByEntity(EntityCombustByEntityEvent event) {
    }

    public void onEntityDamage(EntityDamageEvent event) {
    }

    public void onEntityDamageByBlock(EntityDamageEvent event) {
    }

    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
    }

    public void onEntityDeath(EntityDeathEvent event) {
    }

    public void onEntityDropItem(EntityDropItemEvent event) {
    }

    public void onEntityEnterBlock(EntityEnterBlockEvent event) {
    }

    public void onEntityExplode(EntityExplodeEvent event) {
    }

    public void onEntityInteract(EntityInteractEvent event) {
    }

    public void onEntityPickupItem(EntityPickupItemEvent event) {
    }

    public void onEntityPortal(EntityPortalEvent event) {
    }

    public void onEntityPortalExit(EntityPortalExitEvent event) {
    }

    public void onEntityPoseChange(EntityPoseChangeEvent event) {
    }

    public void onEntityPotionEffect(EntityPotionEffectEvent event) {
    }

    public void onEntityRegainHealth(EntityRegainHealthEvent event) {
    }

    public void onEntityResurrect(EntityResurrectEvent event) {
    }

    public void onEntityShootBow(EntityShootBowEvent event) {
    }

    public void onEntitySpawn(EntitySpawnEvent event) {
    }

    public void onEntityTame(EntityTameEvent event) {
    }

    public void onEntityTarget(EntityTargetEvent event) {
    }

    public void onEntityTargetLivingEntity(EntityTargetLivingEntityEvent event) {
    }

    public void onEntityTeleport(EntityTeleportEvent event) {
    }

    public void onEntityToggleGlide(EntityToggleGlideEvent event) {
    }

    public void onEntityToggleSwim(EntityToggleSwimEvent event) {
    }

    public void onEntityTransform(EntityTransformEvent event) {
    }

    public void onEntityUnleash(EntityUnleashEvent event) {
    }

    public void onExpBottle(ExpBottleEvent event) {
    }

    public void onExplosionPrime(ExplosionPrimeEvent event) {
    }

    public void onFireworkExplode(FireworkExplodeEvent event) {
    }

    public void onFluidLevelChange(FluidLevelChangeEvent event) {
    }

    public void onFoodLevelChange(FoodLevelChangeEvent event) {
    }

    public void onFurnaceBurn(FurnaceBurnEvent event) {
    }

    public void onFurnaceExtract(FurnaceExtractEvent event) {
    }

    public void onFurnaceSmelt(FurnaceSmeltEvent event) {
    }

    public void onHanging(HangingEvent event) {
    }

    public void onHangingBreak(HangingBreakEvent event) {
    }

    public void onHangingBreakByEntity(HangingBreakByEntityEvent event) {
    }

    public void onHangingPlace(HangingPlaceEvent event) {
    }

    public void onHorseJump(HorseJumpEvent event) {
    }

    public void onInventory(InventoryEvent event) {
    }

    public void onInventoryClick(InventoryClickEvent event) {
    }

    public void onInventoryClose(InventoryCloseEvent event) {
    }

    public void onInventoryCreative(InventoryCreativeEvent event) {
    }

    public void onInventoryDrag(InventoryDragEvent event) {
    }

    public void onInventoryInteract(InventoryInteractEvent event) {
    }

    public void onInventoryMoveItem(InventoryMoveItemEvent event) {
    }

    public void onInventoryOpen(InventoryOpenEvent event) {
    }

    public void onInventoryPickupItem(InventoryPickupItemEvent event) {
    }

    public void onItemDespawn(ItemDespawnEvent event) {
    }

    public void onItemMerge(ItemMergeEvent event) {
    }

    public void onItemSpawn(ItemSpawnEvent event) {
    }

    public void onLeaveDecay(LeavesDecayEvent event) {
    }

    public void onLightningStrike(LightningStrikeEvent event) {
    }

    public void onLingeringPotionSplash(LingeringPotionSplashEvent event) {
    }

    public void onMapInitialize(MapInitializeEvent event) {
    }

    public void onMoistureChange(MoistureChangeEvent event) {
    }

    public void onNotePlay(NotePlayEvent event) {
    }

    public void onPigZap(PigZapEvent event) {
    }

    public void onPigZombieAnger(PigZombieAngerEvent event) {
    }

    public void onPlayer(PlayerEvent event) {
    }

    public void onPlayerAdvancementDone(PlayerAdvancementDoneEvent event) {
    }

    public void onPlayerAnimation(PlayerAnimationEvent event) {
    }

    public void onPlayerArmorStandManipulate(PlayerArmorStandManipulateEvent event) {
    }

    public void onPlayerBedEnter(PlayerBedEnterEvent event) {
    }

    public void onPlayerBedLeave(PlayerBedLeaveEvent event) {
    }

    public void onPlayerBucket(PlayerBucketEvent event) {
    }

    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
    }

    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
    }

    public void onPlayerChangedMainHand(PlayerChangedMainHandEvent event) {
    }

    public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
    }

    public void onPlayerChannel(PlayerChannelEvent event) {
    }

    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
    }

    public void onPlayerCommandSend(PlayerCommandSendEvent event) {
    }

    public void onPlayerDeath(PlayerDeathEvent event) {
    }

    public void onPlayerDropItem(PlayerDropItemEvent event) {
    }

    public void onPlayerEditBook(PlayerEditBookEvent event) {
    }

    public void onPlayerEggThrow(PlayerEggThrowEvent event) {
    }

    public void onPlayerEntityUnleash(PlayerUnleashEntityEvent event) {
    }

    public void onPlayerExpChange(PlayerExpChangeEvent event) {
    }

    public void onPlayerFish(PlayerFishEvent event) {
    }

    public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
    }

    public void onPlayerInteract(PlayerInteractEvent event) {
    }

    public void onPlayerInteractAtEntity(PlayerInteractAtEntityEvent event) {
    }

    public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
    }

    public void onPlayerItemBreak(PlayerItemBreakEvent event) {
    }

    public void onPlayerItemConsume(PlayerItemConsumeEvent event) {
    }

    public void onPlayerItemDamage(PlayerItemDamageEvent event) {
    }

    public void onPlayerItemHeld(PlayerItemHeldEvent event) {
    }

    public void onPlayerItemMend(PlayerItemMendEvent event) {
    }

    public void onPlayerJoin(PlayerJoinEvent event) {
    }

    public void onPlayerKick(PlayerKickEvent event) {
    }

    public void onPlayerLeashEntity(PlayerLeashEntityEvent event) {
    }

    public void onPlayerLevelChange(PlayerLevelChangeEvent event) {
    }

    public void onPlayerLocaleChange(PlayerLocaleChangeEvent event) {
    }

    public void onPlayerLogin(PlayerLoginEvent event) {
    }

    public void onPlayerMove(PlayerMoveEvent event) {
    }

    public void onPlayerPickupArrow(PlayerPickupArrowEvent event) {
    }

    public void onPlayerQuit(PlayerQuitEvent event) {
    }

    public void onPlayerRecipeDiscover(PlayerRecipeDiscoverEvent event) {
    }

    public void onPlayerRegisterChannel(PlayerRegisterChannelEvent event) {
    }

    public void onPlayerResourcePackStatus(PlayerResourcePackStatusEvent event) {
    }

    public void onPlayerRespawn(PlayerRespawnEvent event) {
    }

    public void onPlayerRiptide(PlayerRiptideEvent event) {
    }

    public void onPlayerShearEntity(PlayerShearEntityEvent event) {
    }

    public void onPlayerStatisticIncrement(PlayerStatisticIncrementEvent event) {
    }

    public void onPlayerSwapHandItems(PlayerSwapHandItemsEvent event) {
    }

    public void onPlayerTakeLecternBook(PlayerTakeLecternBookEvent event) {
    }

    public void onPlayerToggleFlight(PlayerToggleFlightEvent event) {
    }

    public void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
    }

    public void onPlayerToggleSpring(PlayerToggleSprintEvent event) {
    }

    public void onPlayerUnregisterChannel(PlayerUnregisterChannelEvent event) {
    }

    public void onPlayerVelocity(PlayerVelocityEvent event) {
    }

    public void onPlugin(PluginEvent event) {
    }

    public void onPluginDisable(PluginDisableEvent event) {
    }

    public void onPluginEnable(PluginEnableEvent enable) {
    }

    public void onPortalCreate(PortalCreateEvent event) {
    }

    public void onPotionSplash(PotionSplashEvent event) {
    }

    public void onPrepareAnvil(PrepareAnvilEvent event) {
    }

    public void onPrepareItemCraft(PrepareItemCraftEvent event) {
    }

    public void onPrepareItemEnchant(PrepareItemEnchantEvent event) {
    }

    public void onProjectileHit(ProjectileHitEvent event) {
    }

    public void onProjectileLaunch(ProjectileLaunchEvent event) {
    }

    public void onRaid(RaidEvent event) {
    }

    public void onRaidFinish(RaidFinishEvent event) {
    }

    public void onRaidSpawnWave(RaidSpawnWaveEvent event) {
    }

    public void onRaidStop(RaidStopEvent event) {
    }

    public void onRaidTrigger(RaidTriggerEvent event) {
    }

    public void onRemoteServerCommand(RemoteServerCommandEvent event) {
    }

    public void onServer(ServerEvent event) {
    }

    public void onServerCommand(ServerCommandEvent event) {
    }

    public void onServerListPing(ServerListPingEvent event) {
    }

    public void onServerLoad(ServerLoadEvent event) {
    }

    public void onService(ServiceEvent event) {
    }

    public void onServiceRegister(ServiceRegisterEvent event) {
    }

    public void onServiceUnregister(ServiceUnregisterEvent event) {
    }

    public void onSheepDyeWool(SheepDyeWoolEvent event) {
    }

    public void onSheepRegrowWool(SheepRegrowWoolEvent event) {
    }

    public void onSignChange(SignChangeEvent event) {
    }

    public void onSlimeSplit(SlimeSplitEvent event) {
    }

    public void onSpawnChange(SpawnChangeEvent event) {
    }

    public void onSpongeAbsorb(SpongeAbsorbEvent event) {
    }

    public void onStructureGrow(StructureGrowEvent event) {
    }

    public void onTabComplete(TabCompleteEvent event) {
    }

    public void onThunderChange(ThunderChangeEvent event) {
    }

    public void onTimeSkip(TimeSkipEvent event) {
    }

    public void onTradeSelect(TradeSelectEvent event) {
    }

    public void onVehicle(VehicleEvent event) {
    }

    public void onVehicleBlockCollision(VehicleBlockCollisionEvent event) {
    }

    public void onVehicleCollision(VehicleCollisionEvent event) {
    }

    public void onVehicleCreate(VehicleCreateEvent event) {
    }

    public void onVehicleDamage(VehicleDamageEvent event) {
    }

    public void onVehicleDestroy(VehicleDestroyEvent event) {
    }

    public void onVehicleEnter(VehicleEnterEvent event) {
    }

    public void onVehicleEntityCollision(VehicleEntityCollisionEvent event) {
    }

    public void onVehicleExit(VehicleExitEvent event) {
    }

    public void onVehicleMove(VehicleMoveEvent event) {
    }

    public void onVehicleUpdate(VehicleUpdateEvent event) {
    }

    public void onVillagerAcquireTrade(VillagerAcquireTradeEvent event) {
    }

    public void onVillagerCareerChange(VillagerCareerChangeEvent event) {
    }

    public void onVillagerReplenishTrade(VillagerReplenishTradeEvent event) {
    }

    public void onWeather(WeatherEvent event) {
    }

    public void onWeatherChange(WeatherChangeEvent event) {
    }

    public void onWorld(WorldEvent event) {
    }

    public void onWorldInit(WorldInitEvent event) {
    }

    public void onWorldLoad(WorldLoadEvent event) {
    }

    public void onWorldSave(WorldSaveEvent event) {
    }

    public void onWorldUnload(WorldUnloadEvent event) {
    }

}
