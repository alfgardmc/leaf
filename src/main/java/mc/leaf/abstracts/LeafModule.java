package mc.leaf.abstracts;

import mc.leaf.LeafPlugin;
import mc.leaf.interfaces.IModule;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.logging.Logger;

public abstract class LeafModule extends BukkitListener implements IModule {

    private final LeafPlugin plugin;
    private final String moduleName;

    protected LeafModule(LeafPlugin plugin, String moduleName) {
        this.plugin = plugin;
        this.moduleName = moduleName;
    }

    @Override
    public final void enable(boolean startup) {
        this.getLogger().info(String.format("Enabling module %s...", this.getName()));
        try {
            this.onEnable();
            this.getLogger().info(String.format("Module '%s' enabled.", this.getName()));
        } catch (Exception e) {
            e.printStackTrace();
            this.getLogger().warning(String.format("Failed to enable '%s' module.", this.getName()));
        }
    }

    @Override
    public final void disable(boolean shutdown) {
        this.getLogger().info(String.format("Disabling module %s...", this.getName()));
        try {
            this.onDisable();
            this.getLogger().info(String.format("Module '%s' disabled.", this.getName()));
        } catch (Exception e) {
            e.printStackTrace();
            this.getLogger().warning(String.format("Failed to disable '%s' module.", this.getName()));
        }
    }

    @Override
    public void onEnable() throws Exception {

    }

    @Override
    public void onDisable() throws Exception {

    }

    @Override
    public final String getName() {
        return this.moduleName;
    }

    public final LeafPlugin getPlugin() {
        return plugin;
    }

    public final void sendMessage(String message) {
        this.sendMessage(message, Bukkit.getOnlinePlayers());
    }

    public final void sendMessage(String message, Collection<? extends Player> receivers) {
        Bukkit.getConsoleSender().sendMessage(String.format("§6§l[%s] §r%s", this.getName(), message));
        receivers.forEach(player -> player.sendMessage(String.format("§6§l[%s] §r%s", this.getName(), message)));
    }

    public final void sendMessage(String message, Player player) {
        Bukkit.getConsoleSender().sendMessage(String.format("[%s] §7To '%s' > §r%s", this.getName(), player.getName(), message));
        player.sendMessage(String.format("§6§l[%s] §r%s", this.getName(), message));
    }

    public Logger getLogger() {
        return this.getPlugin().getLogger();
    }

    public void info(String message) {
        this.getLogger().info(String.format("[%s] " + message, this.getName()));
    }
}
