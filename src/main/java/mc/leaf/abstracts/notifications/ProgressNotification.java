package mc.leaf.abstracts.notifications;

import mc.leaf.LeafPlugin;
import mc.leaf.enums.NotificationState;
import mc.leaf.interfaces.notifications.ITaskNotification;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.KeyedBossBar;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

public abstract class ProgressNotification implements ITaskNotification {

    private JavaPlugin plugin;
    private NotificationState state;
    private NamespacedKey key;
    private KeyedBossBar bossBar;
    private int tpsBuildUp = 0;

    public ProgressNotification(LeafPlugin plugin) {
        this.plugin = plugin;
        this.state = NotificationState.WAITING;

        while (this.key == null || Bukkit.getBossBar(this.key) != null) {
            this.key = plugin.getRandomKey();
        }

        this.bossBar = Bukkit.createBossBar(this.key, this.getText(), BarColor.RED, BarStyle.SOLID);
    }

    public boolean continuousDisplay() {
        return false;
    }

    /**
     * Tell the current object entity to cancel what it's actually doing.
     */
    @Override
    public final void cancel() {
        this.setState(NotificationState.CANCELLED);
    }

    @Override
    public final void onCancel() {
    }

    @Override
    public NotificationState getState() {
        return state;
    }

    @Override
    public final void setState(NotificationState state) {
        if (this.state == state) {
            return;
        }

        this.state = state;

        switch (this.state) {
            case RUNNING:
                this.onTaskStarted();
                break;
            case FINISHED:
                this.onTaskFinished();
                break;
            case CANCELLED:
                this.onTaskCancelled();
                break;
        }
    }

    /**
     * Display the notification to players.
     */
    @Override
    public final void show() {
        this.bossBar.setVisible(true);
    }

    /**
     * Hide the notification from players.
     */
    @Override
    public final void hide() {
        this.bossBar.setVisible(false);
    }

    /**
     * Dispose of every resource that need to be freed.
     * (Ex: Removing a BossBar that was used to show the notification)
     */
    @Override
    public final void dispose() {
        if (this.state != NotificationState.CANCELLED && this.state != NotificationState.FINISHED) {
            this.setState(NotificationState.CANCELLED);
        }
        this.hide();
        Bukkit.removeBossBar(this.getKey());
    }

    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public final void run() {
        double tps = Bukkit.getTPS()[0];
        tpsBuildUp++;

        if (tpsBuildUp >= ((20 - tps) + 1)) {
            double p = this.getProgression();

            if (p > 1) p = 1;
            if (p < 0) p = 0;

            this.getBossBar().setProgress(p);

            this.tick();
            this.tpsBuildUp = 0;
        }
    }

    /**
     * Return the namespaced identifier for this object.
     *
     * @return this object's key
     */
    @NotNull
    @Override
    public final NamespacedKey getKey() {
        return this.key;
    }

    public final KeyedBossBar getBossBar() {
        return bossBar;
    }

}
