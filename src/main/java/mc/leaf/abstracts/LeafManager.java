package mc.leaf.abstracts;

import mc.leaf.LeafPlugin;
import mc.leaf.interfaces.IManager;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

public abstract class LeafManager<T extends Runnable> implements IManager<T> {

    public BlockingDeque<T> repeatingEntityList = new LinkedBlockingDeque<>();
    private LeafPlugin leaf;
    private BukkitTask task;

    public LeafManager(LeafPlugin leaf) {
        this.leaf = leaf;
    }

    /**
     * Offer an entity to this manager.
     *
     * @param offer The entity to offer
     */
    @Override
    public void offer(T offer) {
        this.repeatingEntityList.offer(offer);
    }

    /**
     * Start the manager.
     */
    @Override
    public void start() {
        this.task = Bukkit.getScheduler().runTaskTimer(this.leaf, this, 0L, 1L);
        this.onStart();
    }

    /**
     * Stop the manager.
     */
    @Override
    public void stop() {
        Bukkit.getScheduler().cancelTask(this.task.getTaskId());
        this.task = null;
        this.onStop();
    }

}
