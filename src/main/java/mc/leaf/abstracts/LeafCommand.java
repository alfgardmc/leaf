package mc.leaf.abstracts;

import mc.leaf.lib.CommandData;
import mc.leaf.lib.LeafExecutor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

public abstract class LeafCommand implements CommandExecutor {

    private CommandData data;

    public LeafCommand() {
        this.data = new CommandData(this);
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        String commandString = String.join(" ", Arrays.asList(args));

        LeafExecutor executor;
        try {
            executor = this.data.getExecutor(commandString);
            if (executor == null) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        try {
            executor.execute(sender);
        } catch (InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
