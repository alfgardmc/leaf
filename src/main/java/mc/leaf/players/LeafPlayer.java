package mc.leaf.players;

import mc.leaf.LeafPlugin;
import mc.leaf.lib.Storage;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LeafPlayer extends Storage {

    private LeafPlugin plugin;
    private Player player;
    private Map<String, String> properties = new HashMap<>();

    public LeafPlayer(LeafPlugin plugin, Player player) throws Exception {
        super();
        this.plugin = plugin;
        this.player = player;
    }

    @Override
    public String store(String name, String value) throws Exception {
        return this.properties.put(name, value);
    }

    public void save() {
        this.properties.forEach((name, value) -> {
            try {
                super.store(name, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public Optional<String> retrieve(String name) throws Exception {
        if (this.properties.containsKey(name)) {
            return Optional.of(name);
        }
        Optional<String> content = super.retrieve(name);
        content.ifPresent(s -> this.properties.put(name, s));
        return content;
    }

    @Override
    public File getStorageFile() {
        File pluginStorage = new File(this.plugin.getDataFolder(), "players");

        if (!pluginStorage.exists()) {
            pluginStorage.mkdir();
        }

        return new File(pluginStorage, this.player.getUniqueId().toString() + ".db");
    }
}
