package mc.leaf.enums;

import mc.leaf.interfaces.notifications.ITaskNotification;

/**
 * Enumeration representing the current state of a notification.
 * <p>
 * Mainly used to keep track of which notification shouldn't be kept in memory, these can be used to call different
 * method within a notification to trigger some behaviour.
 */
public enum NotificationState {

    /**
     * Use this state when a notification should be kept in memory but shouldn't be display and refreshed yet.
     */
    WAITING(true),

    /**
     * Use this state when a notification is running and should be kept in memory.
     * This will also call {@link ITaskNotification#tick()} method when needed.
     */
    RUNNING(true),

    /**
     * Use this state when a notification is finished and shouldn't be kept in memory.
     */
    FINISHED(false),

    /**
     * Use this state when a notification has been cancelled and shouldn't be kept in memory.
     */
    CANCELLED(false);

    /**
     * If true, the Notification having this state will be kept in memory.
     */
    public boolean postpone;

    /**
     * NotificationState constructor.
     *
     * @param postpone If true, the Notification having this state will be kept in memory.
     */
    NotificationState(boolean postpone) {
        this.postpone = postpone;
    }

}
